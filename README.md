# ansible_monitored_debian
This ansible playbook config sets up a production environment for the iotboat api.
It is tested on a Debian 10 (Buster) server.

### 0. Get Debian 10 server and register dns records
### 1. Fill variables in "group_vars/all"

### 2. Add server ip address + domain name to "./hosts"

### 3. Generate keypair to be used by gitlab-ci for deployments
The public key will be copied to the server during execution.
```bash
./generate_keypair.sh
```

### 4. Connect via vpn to the trusted_proxy_ip

If you skip this step, ufw might block your ssh session during execution

### 5. Install ansible dependencies
```bash
ansible-galaxy collection install -r requirements.yml
```

### 6. Run ansible playbook
```bash
ansible-playbook -i hosts site.yml
```
