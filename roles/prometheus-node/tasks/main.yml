---
- name: "Configure ufw to allow prometheus on port {{ exposed_prometheus_port }}"
  ufw:
    rule: allow
    proto: tcp
    from_ip: "{{ grafana_server_ip }}"
    to_port: "{{ exposed_prometheus_port }}"
    comment: Grafana -> prometheus
  tags: [ prometheus ]

- name: Add group prometheus
  group:
    name: prometheus
    state: present
  tags: [ prometheus ]

- name: Add user prometheus
  user:
    name: prometheus
    group: prometheus
    shell: /bin/false
    create_home: no
    state: present
  tags: [ prometheus ]

- name: Create /etc/prometheus directory
  file:
    path: /etc/prometheus
    state: directory
  tags: [ prometheus ]

- name: Create /var/lib/prometheus directory
  file:
    path: /var/lib/prometheus
    state: directory
  tags: [ prometheus ]

- name: Set ownership of /etc/prometheus
  file:
    path: /etc/prometheus
    state: directory
    recurse: yes
    owner: prometheus
    group: prometheus
  tags: [ prometheus]

- name: Set ownership of /var/lib/prometheus
  file:
    path: /var/lib/prometheus
    state: directory
    recurse: yes
    owner: prometheus
    group: prometheus
  tags: [ prometheus]

- name: Download Prometheus
  get_url:
    url: https://github.com/prometheus/prometheus/releases/download/v2.26.0/prometheus-2.26.0.linux-amd64.tar.gz
    dest: /root/prometheus-2.26.0.linux-amd64.tar.gz
    checksum: "sha256:8dd6786c338dc62728e8891c13b62eda66c7f28a01398869f2b3712895b441b9"

- name: Unpack Prometheus
  unarchive:
    src: /root/prometheus-2.26.0.linux-amd64.tar.gz
    dest: /root/
    remote_src: yes
    creates: /root/prometheus-2.26.0.linux-amd64
  tags: [ prometheus]

- name: Remove downloaded prometheus archive
  file:
    path: /root/prometheus-2.26.0.linux-amd64.tar.gz
    state: absent
  tags: [ prometheus]

- name: Copy prometheus binary
  copy:
    remote_src: yes
    src: /root/prometheus-2.26.0.linux-amd64/prometheus
    dest: /usr/local/bin/prometheus
    owner: prometheus
    group: prometheus
    mode: 0755
  tags: [ prometheus]

- name: Copy promtool binary
  copy:
    remote_src: yes
    src: /root/prometheus-2.26.0.linux-amd64/promtool
    dest: /usr/local/bin/promtool
    owner: prometheus
    group: prometheus
    mode: 0755
  tags: [ prometheus]

- name: Copy consoles
  copy:
    remote_src: yes
    src: /root/prometheus-2.26.0.linux-amd64/consoles
    dest: /etc/prometheus/consoles
    owner: prometheus
    group: prometheus
    mode: 0755
  tags: [ prometheus]

- name: Copy console_libraries
  copy:
    remote_src: yes
    src: /root/prometheus-2.26.0.linux-amd64/console_libraries
    dest: /etc/prometheus/console_libraries
    owner: prometheus
    group: prometheus
    mode: 0755
  tags: [ prometheus]

- name: Remove tmp prometheus folder
  file:
    path: /root/prometheus-2.26.0.linux-amd64
    state: absent
  tags: [ prometheus]

- name: Set up prometheus config
  template:
    src: "prometheus.yml.j2"
    dest: "/etc/prometheus/prometheus.yml"
    owner: prometheus
    group: prometheus
    mode: 0640
  tags: [ prometheus]

- name: Set up prometheus service
  template:
    src: "prometheus.service.j2"
    dest: "/etc/systemd/system/prometheus.service"
    owner: root
    group: root
    mode: 0640
  tags: [ prometheus]

- name: Enable prometheus service
  systemd:
    name: prometheus
    enabled: yes
    daemon_reload: yes
  tags: [ prometheus ]

- name: Start prometheus service
  service:
    name: prometheus
    state: started
  tags: [ prometheus ]

- name: Add group node_exporter
  group:
    name: node_exporter
    state: present
  tags: [ prometheus ]

- name: Add user node_exporter
  user:
    name: node_exporter
    group: node_exporter
    shell: /bin/false
    create_home: no
    state: present
  tags: [ prometheus ]

- name: Download node_exporter
  get_url:
    url: https://github.com/prometheus/node_exporter/releases/download/v1.1.2/node_exporter-1.1.2.linux-amd64.tar.gz
    dest: /root/node_exporter-1.1.2.linux-amd64.tar.gz
    checksum: "sha256:8c1f6a317457a658e0ae68ad710f6b4098db2cad10204649b51e3c043aa3e70d"

- name: Unpack node_exporter
  unarchive:
    src: /root/node_exporter-1.1.2.linux-amd64.tar.gz
    dest: /root/
    remote_src: yes
    creates: /root/node_exporter-1.1.2.linux-amd64
  tags: [ prometheus ]

- name: Remove downloaded node_exporter archive
  file:
    path: /root/node_exporter-1.1.2.linux-amd64.tar.gz
    state: absent
  tags: [ prometheus ]

- name: Copy node_exporter binary
  copy:
    remote_src: yes
    src: /root/node_exporter-1.1.2.linux-amd64/node_exporter
    dest: /usr/local/bin/node_exporter
    owner: node_exporter
    group: node_exporter
    mode: 0755
  tags: [ prometheus ]

- name: Remove tmp node_exporter folder
  file:
    path: /root/node_exporter-1.1.2.linux-amd64
    state: absent
  tags: [ prometheus ]

- name: Set up node_exporter service
  template:
    src: "node_exporter.service.j2"
    dest: /etc/systemd/system/node_exporter.service
    owner: root
    group: root
    mode: 0640
  tags: [ prometheus ]

- name: Enable node_exporter service
  systemd:
    name: node_exporter
    enabled: yes
    daemon_reload: yes
  tags: [ prometheus ]

- name: Start node_exporter service
  service:
    name: node_exporter
    state: started
  tags: [ prometheus ]